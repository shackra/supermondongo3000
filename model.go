package main

import (
	"encoding/json"

	"go.mongodb.org/mongo-driver/bson"
)

// How the DB records looks like

type Customer struct {
	ID          string      `json:"id" bson:"_id"`
	Name        string      `json:"name" bson:"name"`
	LastName    string      `json:"last_name" bson:"last_name"`
	DOB         DOBTime     `json:"dob" bson:"dob"`
	Email       string      `json:"email" bson:"email"`
	PhoneNumber json.Number `json:"phone_number,Number" bson:"phone_number"`
}

func (c *Customer) ToBson() bson.M {
	return bson.M{
		"name":         c.Name,
		"last_name":    c.LastName,
		"dob":          c.DOB,
		"email":        c.Email,
		"phone_number": c.PhoneNumber,
	}
}
