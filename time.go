package main

import (
	"bytes"
	"time"

	"go.mongodb.org/mongo-driver/bson"
)

// provide a custom time type for dealing with birthdays

type DOBTime struct {
	time.Time
}

func (d *DOBTime) UnmarshalJSON(data []byte) error {
	// remove any double quotes
	data = bytes.ReplaceAll(data, []byte(`"`), []byte(``))
	// convert byte array to string
	buff := bytes.NewBuffer(data)
	date := buff.String()
	t, err := time.Parse(`2006-01-02`, date)
	if err != nil {
		t, err = time.Parse(`2006-02-01`, date)
		if err != nil {
			t, err = time.Parse(`02/01/06`, date)
			if err != nil {
				return err
			}
		}
	}
	d.Time = t
	return err
}

func (d DOBTime) MarshalJSON() ([]byte, error) {
	return d.Time.MarshalJSON()
}

func (d DOBTime) GetBSON() (interface{}, error) {
	return d, nil
}

func (d *DOBTime) SetBSON(raw bson.RawValue) error {
	bsonErr := raw.Unmarshal(d.Time)
	if bsonErr != nil {
		return nil
	}
	return nil
}
