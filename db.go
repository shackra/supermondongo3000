package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// define abstraction and structs for communication with a database

type DB interface {
	CreateCustomer(c Customer) error
	GetAllCustomers(w io.Writer) error
}

type MongoDB struct {
	client        *mongo.Client
	serverAddress string
}

func NewMongoClient(s string) *MongoDB {
	return &MongoDB{serverAddress: s}
}

// Connect connects to a MongoDB database
func (m *MongoDB) Connect(ctx context.Context) error {
	var err error
	m.client, err = mongo.NewClient(options.Client().ApplyURI(m.serverAddress))
	if err != nil {
		return err
	}
	err = m.client.Connect(ctx)
	return err
}

// Disconnect disconnects the client from the MongoDB database
func (m *MongoDB) Disconnect() error {
	return m.client.Disconnect(nil)
}

// CreateCustomer inserts a new customer into the database
func (m *MongoDB) CreateCustomer(c Customer) error {
	collection := m.client.Database("db").Collection("customers")
	insert := c.ToBson()
	_, err := collection.InsertOne(nil, insert)
	if err != nil {
		return fmt.Errorf("cannot insert record: %v", err)
	}
	return nil
}

// GetAllCustomers send a JSON array of customers
func (m *MongoDB) GetAllCustomers(w io.Writer) error {
	enc := json.NewEncoder(w)
	// make it look pretty
	enc.SetIndent("", "  ")
	// look for the collection
	collection := m.client.Database("db").Collection("customers")
	cur, err := collection.Find(nil, bson.D{{}})
	if err != nil {
		return err
	}
	// store all customers in an array
	var customers []Customer
	for cur.Next(nil) {
		var c Customer
		err = cur.Decode(&c)
		if err != nil {
			return err
		}

		customers = append(customers, c)
	}
	if len(customers) < 1 {
		return fmt.Errorf("nothing found")
	}
	// encode the array to valid JSON and send it
	err = enc.Encode(&customers)
	return err
}
