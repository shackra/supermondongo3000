package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"time"
)

func main() {
	// create a Mongo client
	mongo := NewMongoClient(os.Getenv("MONGO_HOST"))
	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	// make it connect with the database
	defer cancel()
	err := mongo.Connect(ctx)
	if err != nil {
		panic(err)
	}
	// shutdown the connection on exit
	defer mongo.Disconnect()
	// create the router for the HTTP server
	api := NewAPI(mongo)

	// Create a HTTP server
	s := http.Server{
		Addr:         ":8080",
		Handler:      api,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}
	log.Fatal(s.ListenAndServe())
}
