package main

// define handlers for HTTP requests

import (
	"encoding/json"
	"net/http"
)

// API is the entry point for handling HTTP requests
type API struct {
	db DB
}

func (a *API) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		err := a.db.GetAllCustomers(w)
		if err != nil {
			statusCode := http.StatusInternalServerError
			if err.Error() == "nothing found" {
				statusCode = http.StatusNotFound
			}
			http.Error(w, err.Error(), statusCode)
		}
	case http.MethodPost:
		var c Customer
		err := json.NewDecoder(r.Body).Decode(&c)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		err = a.db.CreateCustomer(c)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		http.Error(w, ``, http.StatusCreated)
	default:
		http.Error(w, `Method Not Allowed`, http.StatusMethodNotAllowed)
	}
}

func NewAPI(db DB) *API {
	return &API{db: db}
}
