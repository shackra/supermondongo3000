package main

import (
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

// test the HTTP Router

// MockDB implements DB interface for testing
type MockDB struct {
	Error error
}

func (m *MockDB) CreateCustomer(c Customer) error {
	return m.fireError()
}

func (m *MockDB) GetAllCustomers(w io.Writer) error {
	return m.fireError()
}

// failOnNextCall sets an error to be triggered on the next call of any
// function of the DB interface
func (m *MockDB) failOnNextCall(e error) {
	m.Error = e
}

// fireError returns an error if there is one
func (m *MockDB) fireError() error {
	if m.Error != nil {
		err := m.Error
		// reset the Error field
		m.Error = nil
		return err
	}
	return nil
}

func setUp() (*MockDB, *httptest.Server) {
	mock := &MockDB{}
	api := NewAPI(mock)
	ts := httptest.NewServer(api)
	return mock, ts
}

func TestErrorMethodNotAllowed(t *testing.T) {
	_, ts := setUp()

	defer ts.Close()

	req, err := http.NewRequest(http.MethodPut, ts.URL, nil)
	if err != nil {
		t.Fatal(err)
	}
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	if res.StatusCode != http.StatusMethodNotAllowed {
		t.Fatalf("Server did not respond with expected status code, expected %d got %d", http.StatusMethodNotAllowed, res.StatusCode)
	}
}

func TestPost(t *testing.T) {
	mock, ts := setUp()

	defer ts.Close()

	cases := []struct {
		desc         string
		expectedCode int
		errorTrigger error
	}{
		{
			desc:         "create a client",
			expectedCode: http.StatusCreated,
		},
		{
			desc:         "fail to create a client",
			expectedCode: http.StatusInternalServerError,
			errorTrigger: fmt.Errorf("random error"),
		},
	}
	for _, c := range cases {
		buff := strings.NewReader(`{"name": "Jorge"}`)
		// set error for next call done to the mocked DB
		mock.failOnNextCall(c.errorTrigger)

		t.Run(c.desc, func(t *testing.T) {
			res, err := http.Post(ts.URL, "application/json", buff)
			if err != nil {
				t.Fatal(err)
			}
			if res.StatusCode != c.expectedCode && c.errorTrigger != nil {
				t.Fatalf("Server did not respond with expected status code, expected %d got %d", c.expectedCode, res.StatusCode)
			}
		})
	}
}

func TestGet(t *testing.T) {
	mock, ts := setUp()

	defer ts.Close()

	cases := []struct {
		desc         string
		expectedCode int
		errorTrigger error
	}{
		{
			desc:         "get a client",
			expectedCode: http.StatusOK,
		},
		{
			desc:         "get a 404 when no clients exists",
			expectedCode: http.StatusNotFound,
			errorTrigger: fmt.Errorf("nothing found"),
		},
		{
			desc:         "get an non-specific error",
			expectedCode: http.StatusInternalServerError,
			errorTrigger: fmt.Errorf("something went wrong"),
		},
	}
	for _, c := range cases {
		// set error for next call done to the mocked DB
		mock.failOnNextCall(c.errorTrigger)

		t.Run(c.desc, func(t *testing.T) {
			res, err := http.Get(ts.URL)
			if err != nil {
				t.Fatal(err)
			}
			if res.StatusCode != c.expectedCode && c.errorTrigger != nil {
				t.Fatalf("Server did not respond with expected status code, expected %d got %d", c.expectedCode, res.StatusCode)
			}
		})
	}
}
