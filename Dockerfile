FROM golang:1.13.0-stretch as base

RUN apt-get update -y && DEBIAN_FRONTEND=noninteractive apt-get install -yq \
        build-essential \
        libffi-dev \
        curl \
        make \
        bzr \
        && apt-get clean && rm -rf /var/lib/apt/lists/*

RUN go get github.com/githubnemo/CompileDaemon

WORKDIR /app

COPY . .

RUN go build

FROM base as production

RUN go install ./...

ENTRYPOINT CompileDaemon -log-prefix=false -command="./supermondongo3000"
