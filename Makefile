.PHONY: help
help: ## This help dialog.
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: run
run: ## run the app locally
	docker-compose up

.PHONY: nuke-all
nuke-all: ## remove images and volumes related to all services
	docker-compose down -v --rmi all

.PHONY: build/%
build/%:  ## build a specific service removing its previous image
	docker-compose rm -f -v -s $*
	docker-compose build $*

.PHONY: test
test:  ## run tests
	@go test -v -coverprofile=cover.out
